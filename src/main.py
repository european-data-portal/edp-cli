import requests
import configparser
import sys
import os
from zipfile import ZipFile

config = configparser.ConfigParser()
api_key = None
hub_url = None
p = sys.argv


def main():
    print("Welcome to EDP CLI")
    load_config()
    if len(p) <= 1:
        print('Please provide a parameter')
        sys.exit()

    if p[1] == 'upload-package':
        upload_data_package()

    if p[1] == 'delete-package':
        delete_data_package()


def load_config():
    global api_key
    global hub_url
    c = config.read('../config.cfg')
    if not c:
        c = config.read('config.cfg')
    if not c:
        print('Could not find Config file')
        sys.exit()

    api_key = config.get('basic', 'api_key')
    hub_url = config.get('basic', 'hub_url')


def upload_data_package():
    if len(p) != 4:
        print('Please provide a data file and catalog id')
        sys.exit()
    file_name = p[2]
    catalog_id = p[3]
    with ZipFile(file_name) as package:
        file_list = package.infolist()
        for file in file_list:
            if file.filename.endswith('.ttl'):
                with package.open(file.filename) as dataset:
                    result = put_dataset(dataset, catalog_id)
                    if result:
                        process_put_result(result, package)
                    else:
                        print('Could not upload data. Maybe it exists already.')


def delete_data_package():
    if len(p) != 4:
        print('Please provide a data file and catalog id')
        sys.exit()
    file_name = p[2]
    catalog_id = p[3]
    with ZipFile(file_name) as package:
        file_list = package.infolist()
        for file in file_list:
            if file.filename.endswith('.ttl'):
                delete_dataset(file.filename, catalog_id)


def put_dataset(file, catalog_id):
    print('Processing ' + file.name)
    dataste_id = os.path.splitext(file.name)[0]
    url = get_put_dataset_url(dataste_id, catalog_id)
    data = file.read()
    result = requests.put(url, data=data, headers=get_headers())
    #print(result.text)
    if result.status_code == 201:
        return result.json()
    else:
        return False


def delete_dataset(name, catalog_id):
    print('Deleting ' + name)
    dataste_id = os.path.splitext(name)[0]
    url = get_delete_dataset_url(dataste_id, catalog_id)
    result = requests.delete(url, headers=get_headers())


def process_put_result(result, package):
    distributions = result['distributions']
    file_list = package.infolist()
    for d in distributions:
        title = d['title']['en']
        id = d['id']
        upload_url = d['upload_url']
        for file in file_list:
            file_name = os.path.splitext(file.filename)[0]
            if file_name == title:
                with package.open(file.filename) as file:
                    upload_file(upload_url, file)


def upload_file(upload_url, file):
    print('Processing ' + file.name)
    files = {'file': file}
    response = requests.post(upload_url, files=files)
    if response.status_code == 200:
        print('Uploaded ' + file.name)
    else:
        print('Could not upload ' + file.name)


def get_put_dataset_url(dataset_id, catalog_id):
    return hub_url + '/datasets/' + dataset_id + '?catalogue=' + catalog_id + '&data=true'


def get_delete_dataset_url(dataset_id, catalog_id):
    return hub_url + '/datasets/' + dataset_id + '?catalogue=' + catalog_id


def get_headers():
    headers = {
        'Authorization': api_key,
        'Content-Type': 'text/turtle'
    }
    return headers


main()

