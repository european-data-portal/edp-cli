# European Data Portal - Command Line Interface

Interact with the EDP from your command line! - WORK IN PROGRESS 

## Setup

### Requirements
- Python 3
- Virtualenv

### Installation and Use for Windows (64bit)
- Download dist/edp-cli.exe
- Create a config.cfg file in the same directory (see config.sample.cfg)
- Open CMD or PowerShell and navigate to the directory
- Upload a data package (see example.zip)
```
>  .\edp-cli.exe upload-package ..\example.zip [catalog-id]
```
- Delete the data
```
>  .\edp-cli.exe delete-package ..\example.zip [catalog-id]
```

### Installation for Development
```
$ git clone https://gitlab.com/european-data-portal/edp-cli.git
$ cd edp-cli
$ virtualenv --python=python3 .
$ source bin/activate
$ pip install -r requirements.txt
$ cp config.sample.cfg config.cfg
```
- Edit the config file and enter the API key and the EDP URL

### Build exe
```
$ cd src
$ pyinstaller -F -n edp-cli --distpath ../dist --workpath ../build main.py
```

## Use

### Upload a Data Package

- Prepare a data package as ZIP file (see example.zip)
  - Metadata has to be in Turtle and stored in [dataset-id].ttl
  - dct:title of each distribution has to correspond to the filename without suffix
- Execute the upload command: `python main.py upload-package [path-to-zip] [catalog-id]`
```
$ cd src
$ python main.py upload-package ../example.zip catalog-1
```

### Delete datasets of a Data Package

- `python main.py delete-package [path-to-zip] [catalog-id]`
```
$ cd src
$ python main.py delete-package ../example.zip catalog-1
```
